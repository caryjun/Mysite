html5 boilerplate

HTML5 Boilerplate 帮你构建 快速, 健壮, 并且 适应力强 的web app或网站。 这个小小的源码包集合了100位开发者的经验，你可以将这些经验运用在你的项目中。

HTML5的样板文件：
入门

使用的项目内容概述。

常见问题-常见问题，随着他们的答案。
HTML5标准的核心

    HTML -默认的HTML。

    CSS -默认的CSS。

    JavaScript -默认JavaScript。

    .htaccess -所有的Apache Web服务器配置（参见我们的替代服务器配置）。

    crossdomain.xml -利用跨域请求介绍。

    其他的一切发展！发展才是硬道理！

扩展和自定义HTML5模板-将进一步与样板。
相关的项目

HTML5标准有几个相关的项目，有助于提高各种生产环境的你/应用程序的网站的性能。

    服务器配置-非Apache服务器的配置。

    节点构建脚本-一个功能丰富的咕噜插件。

    Ant构建脚本-原来HTML5模板构建脚本。